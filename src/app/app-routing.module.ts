import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { AdminComponent } from '../app/vistas/admin/admin.component';
import { VistaHomeComponent } from '../app/vistas/admin/vista-home/vista-home.component';
import { LoginComponent } from '../app/components/login/login.component';
import { VistaHomeCatalogoComponent } from '../app/vistas/operario/vista-home-catalogo/vista-home-catalogo.component';
import { VistaFavoriteComponent } from '../app/vistas/operario/vista-favorite/vista-favorite.component';
import { VistaComprasComponent } from './vistas/operario/vista-compras/vista-compras.component';
import { OperarioComponent } from './vistas/operario/operario.component';
import { VistaCuentaComponent } from './vistas/operario/vista-cuenta/vista-cuenta.component';
import { VistaMapaComponent } from './vistas/admin/vista-mapa/vista-mapa.component';
import { VistaPedidosComponent } from './vistas/admin/vista-pedidos/vista-pedidos.component';
import { VistaProductosComponent } from './vistas/admin/vista-productos/vista-productos.component';
import { VistaOperariosComponent } from './vistas/admin/vista-operarios/vista-operarios.component';
import { VistaReciboComponent } from './vistas/admin/vista-recibo/vista-recibo.component';

import { ListaOrderAdminComponent } from '../app/components/lista-order-admin/lista-order-admin.component';
// import { OperarioComponent } from './vistas/operario/operario.component';
import { FinanzasComponent } from './components/finanzas/finanzas.component';
import { OrderStateComponent } from './components/order-state/order-state.component';

const routes: Routes = [
  { path: '', redirectTo: '/login', pathMatch: 'full' },
  { path: 'login', component: LoginComponent },
  {
    path: 'admin', component: AdminComponent,
    children: [
      { path: 'home', component: VistaHomeComponent },
      { path: '', component: VistaHomeComponent },
      { path: 'pedidos', component: VistaPedidosComponent },
      {path: 'productos', component: VistaProductosComponent},
      {path: 'operarios', component: VistaOperariosComponent},
      {path: 'recibo', component: VistaReciboComponent},
      {path: 'listaoperarios', component: ListaOrderAdminComponent},
    ]
  },
  {
    path: 'operario', component: OperarioComponent,
    children: [
      { path: 'catalogo', component: VistaHomeCatalogoComponent },
      { path: 'favoritos', component: VistaFavoriteComponent },
      { path: 'lista-compras', component: VistaComprasComponent},
      { path: 'mi-cuenta', component: VistaCuentaComponent},
      {path: 'mapa', component: VistaMapaComponent},
      { path: 'finanzas', component: FinanzasComponent},
      { path: 'pedidos', component: OrderStateComponent },
    ]
  },
];
// { path: 'catalogue', loadChildren: () => import('./products/catalogue/catalogue.module').then(m => m.CatalogueModule) }];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
