import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import {CommonModule} from '@angular/common';
import { AngularFirestore } from '@angular/fire/firestore';
import { AngularFireModule } from '@angular/fire';
import { AngularFireAuthModule, AngularFireAuth } from '@angular/fire/auth';
import { environment } from 'src/environments/environment';
import { MatInputModule } from '@angular/material/input';
import { MatSelectModule} from '@angular/material/select';
import { MatProgressBarModule } from '@angular/material/progress-bar';
import { MatFormFieldModule} from '@angular/material/form-field';
import { MatCardModule } from '@angular/material/card';
import { MatTabsModule } from '@angular/material/tabs';
import {MatIconModule} from '@angular/material/icon';
import {MatRadioModule} from '@angular/material/radio';
import {MatSlideToggleModule} from '@angular/material/slide-toggle';

import { NgbModule } from '@ng-bootstrap/ng-bootstrap';

import { LoginComponent } from './components/login/login.component';
import { AdminComponent } from './vistas/admin/admin.component';
import { VistaHomeComponent } from './vistas/admin/vista-home/vista-home.component';
import { VistaHomeCatalogoComponent } from './vistas/operario/vista-home-catalogo/vista-home-catalogo.component';
import { CatalogoComponent } from './components/catalogo/catalogo.component';
import { MenuComponent } from './components/menu/menu.component';
import { ReactiveFormsModule, FormsModule } from '@angular/forms';
import { PaginacionPipe } from './pipes/paginacion.pipe';
import { FilterPipe } from './pipes/filter.pipe';
import { ModalComponent } from './components/modal/modal.component';
import { ProgressComponent } from './components/progress/progress.component';
import { NavbarComponent } from './components/navbar/navbar.component';
import { CarritoComponent } from './components/carrito/carrito.component';
import { CarouselComponent } from './components/carousel/carousel.component';
import { FooterComponent } from './components/footer/footer.component';
import { VistaFavoriteComponent } from './vistas/operario/vista-favorite/vista-favorite.component';
import { OperarioComponent } from './vistas/operario/operario.component';
import { VistaComprasComponent } from './vistas/operario/vista-compras/vista-compras.component';
import { VistaCuentaComponent } from './vistas/operario/vista-cuenta/vista-cuenta.component';
import { FinanzasComponent } from './components/finanzas/finanzas.component';

import {MatStepperModule} from '@angular/material/stepper';
import { VistaMapaComponent } from './vistas/admin/vista-mapa/vista-mapa.component';
import {MatButtonModule} from '@angular/material/button';
import { OrderStateComponent } from './components/order-state/order-state.component';
import { FooterAdminComponent } from './components/footer-admin/footer-admin.component';
import { VistaPedidosComponent } from './vistas/admin/vista-pedidos/vista-pedidos.component';
import { VistaProductosComponent } from './vistas/admin/vista-productos/vista-productos.component';
import { FilaProductsComponent } from './components/fila-products/fila-products.component';
import { VistaOperariosComponent } from './vistas/admin/vista-operarios/vista-operarios.component';
import { VistaReciboComponent } from './vistas/admin/vista-recibo/vista-recibo.component';
import { ListaOrderAdminComponent } from './components/lista-order-admin/lista-order-admin.component';

@NgModule({
  declarations: [
    AppComponent,
    LoginComponent,
    AdminComponent,
    VistaHomeComponent,
    VistaHomeCatalogoComponent,
    CatalogoComponent,
    MenuComponent,
    PaginacionPipe,
    FilterPipe,
    ModalComponent,
    ProgressComponent,
    NavbarComponent,
    CarritoComponent,
    CarouselComponent,
    FooterComponent,
    VistaFavoriteComponent,
    VistaComprasComponent,
    OperarioComponent,
    VistaCuentaComponent,
    VistaMapaComponent,
    FinanzasComponent,
    OrderStateComponent,
    FooterAdminComponent,
    VistaPedidosComponent,
    VistaProductosComponent,
    VistaOperariosComponent,
    FilaProductsComponent,
    VistaReciboComponent,
    ListaOrderAdminComponent,
  ],
  imports: [
    AngularFireModule.initializeApp(environment.firebaseConfig),
    BrowserModule,
    AppRoutingModule,
    BrowserAnimationsModule,
    AngularFireAuthModule,
    ReactiveFormsModule,
    FormsModule,
    MatInputModule,
    NgbModule,
    MatSelectModule,
    MatProgressBarModule,
    MatFormFieldModule,
    MatInputModule,
    MatCardModule,
    MatTabsModule,
    MatIconModule,
    MatStepperModule,
    MatRadioModule,
    MatSlideToggleModule,
    MatButtonModule,
    CommonModule
  ],
  providers: [AngularFireAuth, AngularFirestore],
  bootstrap: [AppComponent]
})
export class AppModule { }
export class GoogleMapDemoModule {}

