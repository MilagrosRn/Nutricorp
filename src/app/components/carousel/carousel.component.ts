import { Component, OnInit } from '@angular/core';
import { NgbCarouselConfig } from '@ng-bootstrap/ng-bootstrap';

@Component({
  selector: 'app-carousel',
  templateUrl: './carousel.component.html',
  styleUrls: ['./carousel.component.scss']
})
export class CarouselComponent implements OnInit {

  slider: any[] = [
    {
      name: '',
      img: 'https://user-images.githubusercontent.com/60928490/92062412-4e9f6880-ed5e-11ea-9641-f904d948b72b.png',
      description: ''
    },
    {
      name: '',
      img: 'https://user-images.githubusercontent.com/60928490/92062748-0cc2f200-ed5f-11ea-9a80-f6d2e169ecd2.png',
      description: ''
    },
    {
      name: '',
      img: 'https://user-images.githubusercontent.com/60928490/92062421-552de000-ed5e-11ea-9202-d800d2c81f30.png',
      description: ''
    },
    {
      name: '',
      img: 'https://user-images.githubusercontent.com/60928490/92062426-56f7a380-ed5e-11ea-9c47-838f2c7171d8.png',
      description: ''
    }
  ];

  constructor( private config: NgbCarouselConfig ) { }

  ngOnInit(): void {
  }

}
