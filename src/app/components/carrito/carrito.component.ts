import { Component, OnInit, Input, OnDestroy } from '@angular/core';
import Product from 'src/app/models/products';
import { ProductsService } from 'src/app/services/products.service';
import { Observable } from 'rxjs';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';

@Component({
  selector: 'app-carrito',
  templateUrl: './carrito.component.html',
  styleUrls: ['./carrito.component.scss']
})
export class CarritoComponent implements OnInit, OnDestroy {
  isLinear = false;

  total$: Observable<number>;
  prod: Observable<Product[]>;
  prodObservable: any;
  firstFormGroup: FormGroup;
  secondFormGroup: FormGroup;
  public products;

  ganancia: number;
  precioSugerido: number;
  contadorCant = 1;
  dataPrecio= 0;
  totales = 0;
  mostrarLiquidacion: boolean;

  constructor(public products$: ProductsService, private _formBuilder: FormBuilder) { }

  buildFrom() {
    this.firstFormGroup = this._formBuilder.group({
      firstCtrl: ['', Validators.required]
    });
    this.secondFormGroup = this._formBuilder.group({
      secondCtrl: ['', Validators.required]
    });
  }

  getAddedProducts() {
    this.products$.productObs.subscribe(prod => {
      this.products = prod;
      console.log(prod);

    });
  }

  priceSuggested(precio: number) {
    this.ganancia = precio * 0.25;
    const precioFinal = precio + this.ganancia;
    this.precioSugerido = precioFinal;
  }

  addCant() {
      this.contadorCant += 1;
      console.log(this.contadorCant, ' totales');
  }

  lessCant() {
      this.contadorCant -= 1;
  }

  lessProduct(product: Product) {
    this.products$.lessProduct(product);
    console.log(product);

  }

limpiarCarrito(){
  this.mostrarLiquidacion = !this.mostrarLiquidacion;
  this.products = [];
}

totalProduct(product$: Product){
  this.totales = this.contadorCant * product$.precio;
  return this.totales;
}

  ngOnInit(): void {
    console.log('ngoinit');
    this.prodObservable = this.products$.productObs.subscribe(prod => {
      this.products = prod;
      for (const item of this.products) {

        console.log('contador', this.contadorCant);
        this.dataPrecio += (item.precio * this.contadorCant);
        console.log( item.precio, 'itemprecio');
      }

      console.log('dataprecio', this.dataPrecio);
      console.log('contador', this.contadorCant);
      this.mostrarLiquidacion = this.dataPrecio !== 0;

    });
    this.buildFrom();
  }

  ngOnDestroy() {
    this.prodObservable.unsubscribe();
  }
}
