import { Component, OnInit } from '@angular/core';
import { ProductsService } from '../../services/products.service';
import Product from 'src/app/models/products';
import { FavoritesService } from 'src/app/services/favorites.service';

@Component({
  selector: 'app-catalogo',
  templateUrl: './catalogo.component.html',
  styleUrls: ['./catalogo.component.scss']
})
export class CatalogoComponent implements OnInit {

  constructor(private products$: ProductsService, private favorite$:FavoritesService) { }

  texto: string =  '';
  estadoPositivo: boolean = true;

  category = 'Aceites';
  conserva = 'Conservas';
  detergente = 'Detergentes';
  pasta = 'Pastas';
  jabon = 'Jabones';

  showMe: boolean = false;

  pageSize: number = 20;
  pageNumber: number = 1;
  products: Product[];
  data: number;

  select:boolean= false;

  cambiaEstado() {
    this.texto = (this.estadoPositivo) ?  '' : '*';
    this.estadoPositivo = !this.estadoPositivo;
  }

  toogleTag() {
    this.showMe = !this.showMe;
  }

  addProduct(product: Product): any {
    console.log(product);
    console.log('agrega carrito');
    this.products$.addProduct(product);

  }

  priceSuggested(precio: any) {
    const ganancia = precio * 0.25;
    const precioFinal = precio + ganancia;
    this.data = parseInt(precioFinal);
  }

  addFavorite(product: Product){
    console.log('add catalogo');
    this.favorite$.addFavorite(product);
  }

  lessFavorite(product: Product){
    console.log(product);
    this.favorite$.lessFavorite(product);
  }

  ngOnInit(): void {
    this.products$.getAllProducts().subscribe(elem => {
      this.products = elem;
    })
  }

  }
