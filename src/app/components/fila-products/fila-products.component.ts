import { Component, OnInit, Input } from '@angular/core';
import { ProductsService } from 'src/app/services/products.service';
import Product from 'src/app/models/products';

@Component({
  selector: 'app-fila-products',
  templateUrl: './fila-products.component.html',
  styleUrls: ['./fila-products.component.scss']
})
export class FilaProductsComponent implements OnInit {

  products: Product[];
  precioSugerido: number;
  @Input('data') category

  constructor(private products$: ProductsService) { }

  priceSuggested(precio: number) {
    const ganancia = precio * 0.25;
    const precioFinal = precio + ganancia;
    this.precioSugerido = precioFinal;
  }

  ngOnInit() {
    this.products$.getAllProducts().subscribe(elem => {
      this.products = elem;
    })
  }

}
