import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ListaOrderAdminComponent } from './lista-order-admin.component';

describe('ListaOrderAdminComponent', () => {
  let component: ListaOrderAdminComponent;
  let fixture: ComponentFixture<ListaOrderAdminComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ListaOrderAdminComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ListaOrderAdminComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
