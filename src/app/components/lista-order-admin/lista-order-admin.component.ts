import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-lista-order-admin',
  templateUrl: './lista-order-admin.component.html',
  styleUrls: ['./lista-order-admin.component.scss']
})
export class ListaOrderAdminComponent implements OnInit {

  listita: any[] = [
    {
      name: 'Pedro Sierra',
      dni: '65443312',
      district: 'Barranco'
    },
    {
      name: 'Lucía Sieza',
      dni: '75663243',
      district: 'Chorrillos'
    },
    {
      name: 'Ana Lopez',
      dni: '24667543',
      district: 'Miraflores'
    },
    {
      name: 'Ana Lopez',
      dni: '24667543',
      district: 'Surquillo'
    }
  ]
  constructor() { }

  ngOnInit(): void {
  }

}
