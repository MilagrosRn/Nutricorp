import { Component, OnInit, Input } from '@angular/core';
import { AuthService } from '../../services/auth.service';
import { Router } from '@angular/router';
@Component({
  selector: 'app-navbar',
  templateUrl: './navbar.component.html',
  styleUrls: ['./navbar.component.scss']
})
export class NavbarComponent implements OnInit {

  @Input() hijoNavbar: any;
  constructor( public authSvc: AuthService, private router: Router) { }

  ngOnInit(): void {
  }
  onLogout(): void{
    this.authSvc.logOut();
    console.log('cerro sesion desde master');
}
}

export class FormFieldPrefixSuffixExample {
  hide = true;
}
