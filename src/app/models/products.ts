export default interface Product {
    name: string ;
    categoria: string ;
    precio: number ;
    imgUrl: string ;

}
