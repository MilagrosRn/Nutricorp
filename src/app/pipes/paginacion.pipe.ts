import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
  name: 'paginacion'
})
export class PaginacionPipe implements PipeTransform {

  transform(array: any[], pageSize: number, pageNumber: number) {
    // if (!array.length) return []
    pageSize = pageSize ;
    pageNumber = pageNumber ;
    return array.slice(pageNumber * pageSize, (pageNumber + 1) * pageSize);
  }
}
