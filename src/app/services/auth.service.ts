import { Injectable } from '@angular/core';
import { auth } from 'firebase/app';
import { AngularFireAuth } from '@angular/fire/auth';
import { User } from 'firebase';

@Injectable({
  providedIn: 'root'
})
export class AuthService {
  public user: User;

  constructor(public afAuth: AngularFireAuth) { }
  async login(email: string, password: string) {
    const result = await this.afAuth.signInWithEmailAndPassword(
      email,
      password
    );
    console.log(email, password);
    return result;
  }
  async logOut() {
    try{
      await this.afAuth.signOut();
    } catch (error){
      console.log(error);
    }
  }

}



