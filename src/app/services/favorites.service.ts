import { Injectable } from '@angular/core';
import { Subject, Observable,BehaviorSubject } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class FavoritesService {

  private favoriteAdded$ = new BehaviorSubject([]);
  public favoriteObs = this.favoriteAdded$.asObservable();
  favorite = [];

  constructor() { }

  addFavorite(prod) {
    this.favorite.push(prod);
    this.favoriteAdded$.next(this.favorite);
  }

  lessFavorite(prod) {
    console.log(prod);
    this.favorite = this.favorite.filter(e => e.name !== prod.name);
    this.favoriteAdded$.next(this.favorite);
  }

  getFavorite(): Observable<any> {
    return this.favoriteAdded$.asObservable();
  }
}
