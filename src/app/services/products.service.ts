import { Injectable } from '@angular/core';
import { AngularFirestore, AngularFirestoreCollection, AngularFirestoreDocument } from '@angular/fire/firestore';

import Product from '../models/products';
import { Observable, BehaviorSubject, Subject } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class ProductsService {
  private collection: AngularFirestoreCollection<Product>;
  private allproducts: Observable<Product[]>;
  private productosAdd$ = new BehaviorSubject([]);
  productObs=this.productosAdd$.asObservable();
  productos = [];

  constructor(private firestore: AngularFirestore) { }


  getAllProducts() {
    this.collection = this.firestore.collection<Product>("products")
    this.allproducts = this.collection.valueChanges()
    return this.allproducts;
  }


  addProduct(prod) {
    console.log(prod);
    this.productos.push(prod);
    this.productosAdd$.next(this.productos);
  }

  getProd$(): Observable<any> {
    console.log('getPrdod')
    return this.productosAdd$.asObservable();
  }

  lessProduct(prod){
    console.log(prod)
    this.productos = this.productos.filter(e => e.name !== prod.name);
    this.productosAdd$.next(this.productos)
  }
}
