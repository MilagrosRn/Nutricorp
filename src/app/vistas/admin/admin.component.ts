import { Component, OnInit, ViewChild, ElementRef } from '@angular/core';
import { getInterpolationArgsLength } from '@angular/compiler/src/render3/view/util';
import { AuthService } from '../../services/auth.service';
import { Router } from '@angular/router';

@Component({
  selector: 'app-admin',
  templateUrl: './admin.component.html',
  styleUrls: ['./admin.component.scss']
})
export class AdminComponent implements OnInit {
  constructor( public authSvc: AuthService, private router: Router) { }
  ngOnInit(): void {
  }
  onLogout(): void{
    this.authSvc.logOut();
    console.log('cerro sesion desde master');
}

}
