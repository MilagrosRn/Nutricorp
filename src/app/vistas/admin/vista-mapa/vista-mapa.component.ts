import { Component, OnInit, ViewChild, ElementRef } from '@angular/core';
import { Router } from '@angular/router';

@Component({
  selector: 'app-vista-mapa',
  templateUrl: './vista-mapa.component.html',
  styleUrls: ['./vista-mapa.component.scss']
})
export class VistaMapaComponent implements OnInit {
  path: string;

  title: string;
  constructor(private router: Router) {

    this.path = this.router.url;
    const newTitle = (this.path === '/operario/mapa') ? 'Encuentra Compañeros Cercanos' : 'Zona de pedidos';
    this.title = newTitle;

    console.log(this.title);

  }

  ngOnInit(): void {
  }


  //   iniciarMapa(){
  //   const coordenadas = {lat:-18.011957,lng:-70.2458096};
  //   const mapa = new google.maps.Map(this.myDiv.nativeElement.innerHTML='ji',{
  //     zoom: 10,
  //     center: coordenadas
  //   })
  // }

}
