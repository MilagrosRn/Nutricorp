import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { VistaOperariosComponent } from './vista-operarios.component';

describe('VistaOperariosComponent', () => {
  let component: VistaOperariosComponent;
  let fixture: ComponentFixture<VistaOperariosComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ VistaOperariosComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(VistaOperariosComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
