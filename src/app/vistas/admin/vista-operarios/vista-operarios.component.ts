import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-vista-operarios',
  templateUrl: './vista-operarios.component.html',
  styleUrls: ['./vista-operarios.component.scss']
})
export class VistaOperariosComponent implements OnInit {

  ranking: any[] = [
    {
      num: 1,
      name: 'María Zamora Vargas',
      price: 's/. 4500'
    },
    {
      num: 2,
      name: 'José Martínez Alva',
      price: 's/. 4500'
    },
    {
      num: 3,
      name: 'Ana Cevallos Ramos',
      price: 's/. 4500'
    }
  ]
  constructor() { }

  ngOnInit(): void {
  }

}

