import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-vista-productos',
  templateUrl: './vista-productos.component.html',
  styleUrls: ['./vista-productos.component.scss']
})
export class VistaProductosComponent implements OnInit {

  table: any[] = [
    {
      num: '1.',
      first: 'Detergente',
      second: 'Bolivar Bebé 400 gr.',
      price: 500
    },
    {
      num: '2.',
      first: 'Aceite',
      second: 'Aceite Cocinero 1lt',
      price: 400
    },
    {
      num: '3.',
      first: 'Conservas',
      second: 'Florida Filete',
      price: 400
    },
  ]
  constructor() { }
  category='Aceites'
  category1='Jabones'
  category2='Detergentes';
  category3="Pastas";
  category4="Conservas"

  ngOnInit(): void {
  }

}
