import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { VistaReciboComponent } from './vista-recibo.component';

describe('VistaReciboComponent', () => {
  let component: VistaReciboComponent;
  let fixture: ComponentFixture<VistaReciboComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ VistaReciboComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(VistaReciboComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
