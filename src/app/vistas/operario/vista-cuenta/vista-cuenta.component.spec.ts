import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { VistaCuentaComponent } from './vista-cuenta.component';

describe('VistaCuentaComponent', () => {
  let component: VistaCuentaComponent;
  let fixture: ComponentFixture<VistaCuentaComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ VistaCuentaComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(VistaCuentaComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
