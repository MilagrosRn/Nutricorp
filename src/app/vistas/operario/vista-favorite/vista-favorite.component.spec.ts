import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { VistaFavoriteComponent } from './vista-favorite.component';

describe('VistaFavoriteComponent', () => {
  let component: VistaFavoriteComponent;
  let fixture: ComponentFixture<VistaFavoriteComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ VistaFavoriteComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(VistaFavoriteComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
