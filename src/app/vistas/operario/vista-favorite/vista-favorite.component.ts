import { Component, OnInit } from '@angular/core';

import { FavoritesService } from '../../../services/favorites.service';
import Product from 'src/app/models/products';
import { ProductsService } from 'src/app/services/products.service';

@Component({
  selector: 'app-vista-favorite',
  templateUrl: './vista-favorite.component.html',
  styleUrls: ['./vista-favorite.component.scss']
})
export class VistaFavoriteComponent implements OnInit {

  favorites:Product[] = [];
  data: any;
  favortieObs: any;
  mostrarFavs: boolean = false;

  constructor(private favorites$:FavoritesService, private products$: ProductsService) { }

  priceSuggested(precio: any) {
    const ganancia = precio * 0.25;
    const precioFinal = precio + ganancia;
    this.data = precioFinal;
  }

  addProduct(product: Product) {
    console.log(product);
    this.products$.addProduct(product);
  }

  lessFavorite(product: Product){
    console.log(product);
    this.favorites$.lessFavorite(product);
  }

  ngOnInit(): void {
    this.favortieObs = this.favorites$.getFavorite().subscribe(dataFav =>{
      this.favorites = dataFav;
      console.log(this.favorites);
      // this.mostrarLiquidacion = this.favorites !== []

    })
  }
  ngOnDestroy(){
    this.favortieObs.unsubscribe();
  }

}
