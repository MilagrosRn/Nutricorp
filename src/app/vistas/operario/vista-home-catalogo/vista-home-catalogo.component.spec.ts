import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { VistaHomeCatalogoComponent } from './vista-home-catalogo.component';

describe('VistaHomeCatalogoComponent', () => {
  let component: VistaHomeCatalogoComponent;
  let fixture: ComponentFixture<VistaHomeCatalogoComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ VistaHomeCatalogoComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(VistaHomeCatalogoComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
